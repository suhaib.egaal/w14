

import React, { useState } from 'react';
import './App.css';

function App() {
  const [chatInput, setChatInput] = useState('');

  const sendChat = () => {
    const chats = JSON.parse(localStorage.getItem('chats')) || [];
    chats.push(chatInput);
    localStorage.setItem('chats', JSON.stringify(chats));
    setChatInput('');
  };

  const showMessages = () => {
    const chats = JSON.parse(localStorage.getItem('chats')) || [];
    console.log('Stored Chats:', chats);
  };

  return (
    <div className="App">
      <div className="header">
        <h1 className="black-theme">Chat to localStorage assignment</h1>
        <p className="subtitle">A simple chat application using localStorage</p>
      </div>
      <div className="content">
        <p>
          Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Natoque penatibus et magnis dis parturient. Proin fermentum leo vel orci porta non pulvinar neque. Porttitor rhoncus dolor purus non enim praesent elementum facilisis. Lectus sit amet est placerat in egestas erat imperdiet sed. Eros in cursus turpis massa tincidunt dui. Scelerisque in dictum non consectetur. Eget felis eget nunc lobortis. Ridiculus mus mauris vitae ultricies leo integer malesuada.
        </p>
        <p>
          Consectetur adipiscing elit ut aliquam purus sit. Ipsum a arcu cursus vitae. Vel orci porta non pulvinar neque laoreet suspendisse interdum. Sit amet cursus sit amet dictum sit amet justo donec. Duis convallis convallis tellus id interdum velit laoreet id donec. Sit amet mattis vulputate enim nulla. Semper viverra nam libero justo laoreet sit amet. Eu volutpat odio facilisis mauris sit amet massa vitae. Id donec ultrices tincidunt arcu non sodales neque. Senectus et netus et malesuada fames ac.
        </p>
        <p>
          Aliquam sem et tortor consequat id porta. Et tortor consequat id porta nibh venenatis cras sed felis. Ullamcorper malesuada proin libero nunc consequat. Mus mauris vitae ultricies leo integer malesuada nunc. Egestas pretium aenean pharetra magna. Urna nec tincidunt praesent semper feugiat nibh sed pulvinar proin. A erat nam at lectus urna duis convallis convallis. Viverra mauris in aliquam sem fringilla ut morbi tincidunt.
        </p>
        <p>
          Nullam eget felis eget nunc lobortis mattis aliquam faucibus. Turpis cursus in hac habitasse platea dictumst quisque sagittis purus. Ut sem nulla pharetra diam sit amet nisl. Amet consectetur adipiscing elit pellentesque habitant. Dui id ornare arcu odio ut sem nulla pharetra diam. Venenatis tellus in metus vulputate eu scelerisque felis imperdiet. Urna porttitor rhoncus dolor purus non enim praesent elementum facilisis. Tellus rutrum tellus pellentesque eu tincidunt tortor aliquam nulla. Non blandit massa enim nec dui nunc mattis. Tortor consequat id porta nibh venenatis cras.
        </p>
        <p>
          In nibh mauris cursus mattis molestie a iaculis. Elit at imperdiet dui accumsan sit amet nulla facilisi. Pretium quam vulputate dignissim suspendisse in. Et tortor at risus viverra adipiscing. Rhoncus aenean vel elit scelerisque mauris pellentesque. Et malesuada fames ac turpis egestas. At in tellus integer feugiat scelerisque varius morbi enim. Nulla pellentesque dignissim enim sit amet. Commodo nulla facilisi nullam vehicula ipsum a arcu cursus vitae. Sed viverra tellus in hac habitasse. Tellus at urna condimentum mattis pellentesque id nibh. Feugiat scelerisque varius morbi enim nunc. Pretium quam vulputate dignissim suspendisse in est ante in. Erat pellentesque adipiscing commodo elit at imperdiet dui accumsan.
        </p>
        <p>
          Proin sed libero enim sed faucibus turpis in. Sagittis aliquam malesuada bibendum arcu vitae elementum curabitur vitae. Rhoncus aenean vel elit scelerisque mauris pellentesque pulvinar pellentesque. Montes nascetur ridiculus mus mauris vitae. Ipsum suspendisse ultrices gravida dictum fusce ut. Augue interdum velit euismod in. Leo urna molestie at elementum eu facilisis sed odio morbi. Sapien nec sagittis aliquam malesuada bibendum arcu vitae etense with suppurur. Lacinia ac quis risus sed vulputate odio ut. Massa sed elementum tempus egestas sed sed risus proThis is test Send Vulputate mi sit amet mauris. Gravida arcu ac tortor dignissim convallis aenean et. Viverra vitae conque eu consequat ad Show messages Velit egestas
        </p>
      </div>
      <div className="chatbox">
        <input
          type="text"
          id="chat"
          placeholder="Type your message..."
          value={chatInput}
          onChange={(e) => setChatInput(e.target.value)}
        />
        <button onClick={sendChat}>Send</button>
        <button onClick={showMessages}>Show messages</button>
      </div>
    </div>
  );
}

export default App;
